package net.encomendaz.test.unit.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import junit.framework.Assert;

import org.alfredlibrary.utilitarios.correios.Rastreamento;
import org.alfredlibrary.utilitarios.correios.RegistroRastreamento;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.alienlabz.encomendaz.business.TrackingBC;
import com.alienlabz.encomendaz.domain.State;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.domain.TrackingStatus;
import com.alienlabz.encomendaz.persistence.TrackingPersistence;
import com.alienlabz.encomendaz.persistence.jpa.TrackingPersistenceImpl;

/**
 * Classe de teste unitário para TrackingBC.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Rastreamento.class })
@Ignore
public class TrackingBCTest {

	private TrackingBC trackingBC;

	private TrackingPersistence persistence;

	private Logger logger;

	@Before
	public void before() {
		trackingBC = new TrackingBC();
		persistence = PowerMock.createMock(TrackingPersistenceImpl.class);
		logger = PowerMock.createMock(Logger.class);
		logger.debug(EasyMock.anyObject(String.class));
		EasyMock.expectLastCall().anyTimes();
		Whitebox.setInternalState(trackingBC, "delegate", persistence);
		Whitebox.setInternalState(trackingBC, "logger", logger);
	}

	/**
	 * Ao inserir um novo rastreamento, deve-se colocar a data de inserção como agora.
	 */
	@Test
	public void testInsertDefineDefaultDate() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");
		EasyMock.expectLastCall().andReturn(new ArrayList<RegistroRastreamento>());
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertTrue(tracking.getDate() != null);
		PowerMock.verifyAll();
	}

	/**
	 * Testar se o arquivamento realmente acontece.
	 */
	@Test
	public void testArchive() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");
		tracking.setArchived(false);

		persistence.update(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.archive(tracking);
		Assert.assertTrue(tracking.isArchived());
		PowerMock.verifyAll();
	}

	/**
	 * Toda encomenda que é inserida deve ser colocada como não arquivada.
	 */
	@Test
	public void testInsertDefineNotArchied() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");
		EasyMock.expectLastCall().andReturn(new ArrayList<RegistroRastreamento>());
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertFalse(tracking.isArchived());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a conversão dos registros vindos do Alfred.
	 */
	@Test
	public void testInsertCorrectConversionStatus() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();
		RegistroRastreamento registro = new RegistroRastreamento();
		registro.setAcao("Ação 1");
		registro.setDataHora(new Date());
		registro.setDetalhe("Detalhe");
		registro.setLocal("Salvador");
		registros.add(registro);

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		TrackingStatus status = tracking.getStatuses().iterator().next();
		Assert.assertEquals(registro.getDetalhe(), status.getDetails());
		Assert.assertEquals(registro.getAcao(), status.getStatus());
		Assert.assertEquals(registro.getLocal(), status.getPlace());
		Assert.assertEquals(registro.getDataHora(), status.getDate());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda como tributada.
	 */
	@Test
	public void testInsertSetTaxed() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();
		registros.add(createRegister("Postado", new Date(), "Em trânsito para TRIBUTADO - EMISSÃO NTS", "Salvador"));

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertTrue(tracking.isTaxed());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda com o status de postado.
	 */
	@Test
	public void testInsertSetStatusPosted() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();

		registros.add(createRegister("Postado", new Date(), "Details", "Salvador"));

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertEquals(State.Posted, tracking.getState());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda com o status de encaminhado.
	 */
	@Test
	public void testInsertSetStatusRouted() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();

		registros.add(createRegister("Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Mal Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Postado", new Date(), "Details", "Salvador"));

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertEquals(State.Routed, tracking.getState());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda com o status de mal encaminhado.
	 */
	@Test
	public void testInsertSetStatusBadRouted() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();

		registros.add(createRegister("Mal Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Postado", new Date(), "Details", "Salvador"));

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertEquals(State.BadRouted, tracking.getState());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda com o status de retornado.
	 */
	@Test
	public void testInsertSetStatusReturned() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();

		registros.add(createRegister("Devolvido", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Mal Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Postado", new Date(), "Details", "Salvador"));

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertEquals(State.Returned, tracking.getState());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda com o status de retorno internacional.
	 */
	@Test
	public void testInsertSetStatusInternationalReturn() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();

		registros.add(createRegister("Devolução Internacional", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Devolvido", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Mal Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Postado", new Date(), "Details", "Salvador"));

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertEquals(State.InternationalReturn, tracking.getState());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda com o status de entregue.
	 */
	@Test
	public void testInsertSetStatusDelivered() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();

		registros.add(createRegister("Entregue", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Mal Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Postado", new Date(), "Details", "Salvador"));

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertEquals(State.Delivered, tracking.getState());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda com o status de Conferido.
	 */
	@Test
	public void testInsertSetStatusChecked() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();

		registros.add(createRegister("Conferido", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Aguardando Retirada", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Mal Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Postado", new Date(), "Details", "Salvador"));

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertEquals(State.Checked, tracking.getState());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda com o status de Não Registrado.
	 */
	@Test
	public void testInsertSetStatusUnregistered() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		EasyMock.expectLastCall().andReturn(new ArrayList<RegistroRastreamento>());
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertEquals(State.Unregistered, tracking.getState());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda com o status de Não Procurado.
	 */
	@Test
	public void testInsertSetStatusOnsought() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();

		registros.add(createRegister("Não Procurado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Aguardando Retirada", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Mal Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Postado", new Date(), "Details", "Salvador"));

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertEquals(State.Unsought, tracking.getState());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda com o status de Saiu para Entrega.
	 */
	@Test
	public void testInsertSetStatusOutForDelivery() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();

		registros.add(createRegister("Saiu para Entrega", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Aguardando Retirada", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Mal Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Postado", new Date(), "Details", "Salvador"));

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertEquals(State.OutForDelivery, tracking.getState());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda com o status de em fiscalização pelo exército.
	 */
	@Test
	public void testInsertSetStatusInSupervisionArmy() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();

		registros.add(createRegister("Encaminhado", new Date(), "Fiscalização Exercito", "Salvador"));
		registros.add(createRegister("Aguardando Retirada", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Mal Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Postado", new Date(), "Details", "Salvador"));

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertEquals(State.InSupervisionArmy, tracking.getState());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda com o status de em fiscalização.
	 */
	@Test
	public void testInsertSetStatusInSupervision() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();

		registros.add(createRegister("Encaminhado", new Date(), "Fiscalização", "Salvador"));
		registros.add(createRegister("Aguardando Retirada", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Mal Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Postado", new Date(), "Details", "Salvador"));

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertEquals(State.InSupervision, tracking.getState());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se ocorreu corretamente a definição da encomenda com o status de aguardando retirada.
	 */
	@Test
	public void testInsertSetStatusWaitingReceiver() {
		Tracking tracking = new Tracking();
		tracking.setCode("PB431645789BR");

		PowerMock.mockStatic(Rastreamento.class);
		Rastreamento.rastrear("PB431645789BR");

		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();

		registros.add(createRegister("Aguardando Retirada", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Mal Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Encaminhado", new Date(), "Details", "Salvador"));
		registros.add(createRegister("Postado", new Date(), "Details", "Salvador"));

		EasyMock.expectLastCall().andReturn(registros);
		persistence.insert(tracking);
		EasyMock.expectLastCall().times(1);

		PowerMock.replayAll();
		trackingBC.insert(tracking);
		Assert.assertEquals(State.WaitingReceiver, tracking.getState());
		PowerMock.verifyAll();
	}

	/**
	 * Verificar se encontra corretamente quais os rastreamentos que tiveram mudança de situação.
	 */
	@Test
	public void testVerifyModified() {
		Tracking intransit_tracking_with_changes = createTracking("PB431645789BR");
		intransit_tracking_with_changes.setStatuses(new ArrayList<TrackingStatus>());
		intransit_tracking_with_changes.getStatuses().add(createTrackingStatus("Aguardando Retirada", new Date(), "Details", "Salvador"));

		Tracking intransit_tracking_no_changes = createTracking("AB431645789US");
		intransit_tracking_no_changes.setStatuses(new ArrayList<TrackingStatus>());
		intransit_tracking_no_changes.getStatuses().add(createTrackingStatus("Aguardando Retirada", new Date(), "Details", "Salvador"));

		Tracking unregistered_tracking_with_changes = createTracking("DB431645789PT");
		Tracking unregistered_tracking_no_changes = createTracking("EC431645789CN");

		PowerMock.mockStatic(Rastreamento.class);

		Rastreamento.rastrear("PB431645789BR");
		List<RegistroRastreamento> registros = new ArrayList<RegistroRastreamento>();
		registros.add(createRegister("Entregue", new Date(), "Details", "Salvador"));
		EasyMock.expectLastCall().andReturn(registros);

		Rastreamento.rastrear("AB431645789US");
		registros = new ArrayList<RegistroRastreamento>();
		registros.add(createRegister("Aguardando Retirada", new Date(), "Details", "Salvador"));
		EasyMock.expectLastCall().andReturn(registros);

		Rastreamento.rastrear("DB431645789PT");
		registros = new ArrayList<RegistroRastreamento>();
		registros.add(createRegister("Postado", new Date(), "Details", "Salvador"));
		EasyMock.expectLastCall().andReturn(registros);

		Rastreamento.rastrear("EC431645789CN");
		EasyMock.expectLastCall().andReturn(new ArrayList<RegistroRastreamento>());

		List<Tracking> intransit_trackings = new ArrayList<Tracking>();
		intransit_trackings.add(intransit_tracking_with_changes);
		intransit_trackings.add(intransit_tracking_no_changes);
		List<Tracking> unregistered_trackings = new ArrayList<Tracking>();
		unregistered_trackings.add(unregistered_tracking_with_changes);
		unregistered_trackings.add(unregistered_tracking_no_changes);

		//		EasyMock.expect(persistence.findInTransitTrackings(new SearchBundle())).andReturn(intransit_trackings);
		//		EasyMock.expect(persistence.findUnregisteredTrackings(new SearchBundle())).andReturn(unregistered_trackings);

		persistence.update(EasyMock.anyObject(Tracking.class));
		EasyMock.expectLastCall().times(4);

		PowerMock.replayAll();
		List<Tracking> modified = trackingBC.verifyTrackings();
		PowerMock.verifyAll();

		Assert.assertEquals(2, modified.size());
		Iterator<Tracking> it = modified.iterator();
		Assert.assertEquals("PB431645789BR", it.next().getCode());
		Assert.assertEquals("DB431645789PT", it.next().getCode());
	}

	private TrackingStatus createTrackingStatus(String status, Date date, String details, String place) {
		TrackingStatus trackingStatus = new TrackingStatus();
		trackingStatus.setStatus(status);
		trackingStatus.setDate(date);
		trackingStatus.setDetails(details);
		trackingStatus.setPlace(place);
		return trackingStatus;
	}

	private Tracking createTracking(String code) {
		Tracking tracking = new Tracking();
		tracking.setCode(code);
		return tracking;
	}

	private RegistroRastreamento createRegister(String acao, Date date, String detalhe, String local) {
		RegistroRastreamento registro = new RegistroRastreamento();
		registro.setAcao(acao);
		registro.setDataHora(date);
		registro.setDetalhe(detalhe);
		registro.setLocal(local);
		return registro;

	}
}
