package net.encomendaz.test.integration.business;

import java.util.List;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.gov.frameworkdemoiselle.junit.DemoiselleRunner;
import br.gov.frameworkdemoiselle.transaction.Transactional;

import com.alienlabz.encomendaz.business.TrackingBC;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.util.SearchBundle;

/**
 * Classe de teste para o BusinessController de Rastreamento.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@Transactional
@RunWith(DemoiselleRunner.class)
@Ignore
public class TrackingBCTest {

	@Inject
	private TrackingBC trackingBC;

	@Before
	public void before() {
		for (Tracking tracking : trackingBC.findAll()) {
			trackingBC.delete(tracking.getId());
		}
	}

	@Test
	public void testInsertIncorrectCode() {
		Tracking tracking1 = new Tracking();
		tracking1.setCode("PB4316457891BA");
		try {
			trackingBC.insert(tracking1);
			Assert.fail();
		} catch (ConstraintViolationException exception) { }
		
		tracking1 = new Tracking();
		tracking1.setCode("PB431645789B");
		try {
			trackingBC.insert(tracking1);
			Assert.fail();
		} catch (ConstraintViolationException exception) { }

		tracking1 = new Tracking();
		tracking1.setCode("1B431645789BA");
		try {
			trackingBC.insert(tracking1);
			Assert.fail();
		} catch (ConstraintViolationException exception) { }

	}

	@Test
	public void testCorrectCode() {
		Tracking tracking1 = new Tracking();
		tracking1.setCode("PB431645789BA");
		try {
			trackingBC.insert(tracking1);
		} catch (ConstraintViolationException exception) { 
			Assert.fail();			
		}
	}

	@Test
	public void testFindNotArchivedTrackings() {
		Tracking tracking1 = new Tracking();
		tracking1.setArchived(true);
		tracking1.setCode("PB431645789BA");

		Tracking tracking2 = new Tracking();
		tracking2.setArchived(false);
		tracking2.setCode("PB431645789BR");

		Tracking tracking3 = new Tracking();
		tracking3.setArchived(false);
		tracking3.setCode("PB431645789US");

		trackingBC.insert(tracking1);
		trackingBC.insert(tracking2);
		trackingBC.insert(tracking3);

		List<Tracking> trackings = trackingBC.findNotArchivedTrackings(new SearchBundle());
		Assert.assertEquals(2, trackings.size());

		for (Tracking tracking : trackings) {
			Assert.assertTrue(!tracking.getCode().equals("PB431645789BA"));
		}
	}

}
