package com.alienlabz.encomendaz.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;
import org.simpleframework.xml.Transient;

/**
 * Preferências gerais de uma pessoa. Por exemplo, se ela aceita o envio de
 * e-mails.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@Default(value = DefaultType.FIELD, required = false)
@Entity
public class PersonPreferences {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Transient
	private Long id;

	@Column
	private boolean notifyByMail;

	@Column
	private boolean notifyByTwitter;

	@Column
	private boolean notifyBySMS;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNotifyByMail(boolean notifyByMail) {
		this.notifyByMail = notifyByMail;
	}

	public boolean isNotifyByMail() {
		return notifyByMail;
	}

	public void setNotifyByTwitter(boolean notifyByTwitter) {
		this.notifyByTwitter = notifyByTwitter;
	}

	public boolean isNotifyByTwitter() {
		return notifyByTwitter;
	}

	public void setNotifyBySMS(boolean notifyBySMS) {
		this.notifyBySMS = notifyBySMS;
	}

	public boolean isNotifyBySMS() {
		return notifyBySMS;
	}

}
