package com.alienlabz.encomendaz.domain;

/**
 * Servidores com configuração pré-definida.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public enum EmailServers {
	Outro("Definição do usuario", "", 25, EmailServerSecurity.None), Hotmail("Hotmail", "smtp.live.com", 587, EmailServerSecurity.STARTTLS), Gmail("GMail", "smtp.gmail.com",
			465, EmailServerSecurity.SSL_TLS), Yahoo("Yahoo.br", "smtp.mail.yahoo.com.br", 25, EmailServerSecurity.SSL_TLS), Terra("Terra", "smtp.terra.com.br", 25,
			EmailServerSecurity.None);

	private String description;
	private String smtp;
	private int port;
	private EmailServerSecurity security;

	private EmailServers(String description, String smtp, Integer port, EmailServerSecurity security) {
		this.setDescription(description);
		this.setPort(port);
		this.setSmtp(smtp);
		this.setSecurity(security);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}

	public String getSmtp() {
		return smtp;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getPort() {
		return port;
	}

	public void setSecurity(EmailServerSecurity security) {
		this.security = security;
	}

	public EmailServerSecurity getSecurity() {
		return security;
	}
}
