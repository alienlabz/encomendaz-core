package com.alienlabz.encomendaz.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representa um backup do sistsema.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class Backup {
	private String name;
	private Date date;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	@Override
	public String toString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return dateFormat.format(date) + " - " + name;
	}
	
}
