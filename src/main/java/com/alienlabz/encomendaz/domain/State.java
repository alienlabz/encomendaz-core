package com.alienlabz.encomendaz.domain;

/**
 * Situação em que se encontra uma encomenda.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public enum State {
	Unregistered("Não Registrada"), Returned("Retornada"), InternationalReturn("Retorno Internacional"), WaitingReceiver(
			"Aguardando Retirada"), Unsought("Não procurado"), Delivered("Entregue"), Routed("Encaminhada"), OutForDelivery(
			"Saiu para Entrega"), BadRouted("Rota Errada"), Checked("Checado"), Posted("Postado"), InSupervision(
			"Em Supervisão"), InSupervisionArmy("Em Supervisão - Forças Armadas");
	private String state;

	State(final String state) {
		this.state = state;
	}

	public String toString() {
		return state;
	}
}
