package com.alienlabz.encomendaz.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;

/**
 * Classe que representa uma pessoa que pode receber ou envia encomendas.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@Default(value = DefaultType.FIELD, required = false)
@Entity
public class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String twitter;

	@Column
	@NotNull(message = "{person.null.name}")
	@NotEmpty(message = "{person.null.name}")
	private String name;

	@Column
	@Email(message = "{person.invalid.email}")
	private String email;

	@Column
	private String address1;

	@Column
	private String address2;

	@Column
	private String state;

	@Column
	private String city;

	@Column
	private String country;

	@Column
	private String district;

	@Column
	private String postalCode;

	@ManyToOne(fetch = FetchType.EAGER, optional = true, cascade = CascadeType.ALL)
	@JoinColumn(name = "preferences_id", insertable = true, nullable = true, updatable = true)
	private PersonPreferences preferences;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public PersonPreferences getPreferences() {
		return preferences;
	}

	public void setPreferences(PersonPreferences preferences) {
		this.preferences = preferences;
	}

	@Override
	public String toString() {
		return name;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getTwitter() {
		return twitter;
	}

	public String getCompleteInformations() {
		StringBuilder str = new StringBuilder();
		str.append(name);
		if (postalCode != null) {
			str.append("(");
			str.append(postalCode);
			str.append(")");
		}
		str.append(" - ");
		if (city != null) {
			str.append(city);
			str.append(", ");
		}
		if (state != null) {
			str.append(state);
			str.append(", ");
			if (country != null) {
			}
			str.append(country);
		}
		return str.toString();
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

}