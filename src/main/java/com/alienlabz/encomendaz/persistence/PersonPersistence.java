package com.alienlabz.encomendaz.persistence;

import com.alienlabz.encomendaz.domain.Person;

import br.gov.frameworkdemoiselle.stereotype.PersistenceController;
import br.gov.frameworkdemoiselle.template.Crud;

/**
 * Define o comportamento padrão para a persistência de Pessoas.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@PersistenceController
public interface PersonPersistence extends Crud<Person, Long> {

	/**
	 * Inserir uma pessoa no banco de dados apenas se ela não existir.
	 * Caso exista, nada faz. Não lança exceção.
	 * 
	 * @param person Pessoa a ser inserida.
	 */
	void insertIfNotExists(Person person);

}
