package com.alienlabz.encomendaz.persistence.jpa;

import com.alienlabz.encomendaz.domain.PostalService;

import br.gov.frameworkdemoiselle.stereotype.PersistenceController;
import br.gov.frameworkdemoiselle.template.JPACrud;

/**
 * Trata da persistência dos dados da entidade {@link PostalService} através de JPA.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@PersistenceController
public class PostalServicePersistenceImpl extends JPACrud<PostalService, Long> {

}
