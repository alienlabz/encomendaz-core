package com.alienlabz.encomendaz.persistence.jpa;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;

import com.alienlabz.encomendaz.domain.State;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.persistence.TrackingPersistence;
import com.alienlabz.encomendaz.util.SearchBundle;

import br.gov.frameworkdemoiselle.annotation.Name;
import br.gov.frameworkdemoiselle.stereotype.PersistenceController;
import br.gov.frameworkdemoiselle.template.JPACrud;
import br.gov.frameworkdemoiselle.util.ResourceBundle;
import br.gov.frameworkdemoiselle.util.Strings;

/**
 * Persistência JPA para Postagens.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@SuppressWarnings("unchecked")
@PersistenceController
public class TrackingPersistenceImpl extends JPACrud<Tracking, Long> implements TrackingPersistence {
	private static final long serialVersionUID = 1L;

	@Inject
	@Name("queries")
	private ResourceBundle bundleQueries;

	@Inject
	private Logger logger;

	@Override
	public List<Tracking> findImportantTrackings(final SearchBundle bundle) {
		logger.debug("consultando na base de dados os rastreamentos importantes.");
		String sqlBundle = buildSearchBundle(bundle);
		Query query = getEntityManager().createQuery(bundleQueries.getString("find.important.trackings") + sqlBundle);
		if (!Strings.isEmpty(sqlBundle)) {
			query.setParameter("searchText", "%" + bundle.getSearchText() + "%");
		}
		return query.getResultList();
	}

	@Override
	public List<Tracking> findNotArchivedTrackings(final SearchBundle bundle) {
		logger.debug("consultando na base de dados os rastreamentos não arquivados.");
		String sqlBundle = buildSearchBundle(bundle);
		Query query = getEntityManager()
				.createQuery(bundleQueries.getString("find.not.archived.trackings") + sqlBundle);
		if (!Strings.isEmpty(sqlBundle)) {
			query.setParameter("searchText", "%" + bundle.getSearchText() + "%");
		}
		return query.getResultList();
	}

	/**
	 * Construir o restante da query baseado no bundle.
	 * 
	 * @param bundle Bundle.
	 * @return Query complementar.
	 */
	private String buildSearchBundle(final SearchBundle bundle) {
		StringBuilder query = new StringBuilder();
		if (bundle != null && !Strings.isEmpty(bundle.getSearchText())) {
			query.append(" and (tracking in (select tracking from Tracking tracking where lower(tracking.description) like :searchText");
			query.append(" or lower(tracking.code) like :searchText");
			query.append(" or lower(tracking.details) like :searchText");
			query.append(" or tracking.from in (select person from Person person where lower(person.name) like :searchText or lower(person.country) like :searchText ");
			query.append(" or lower(person.city) like :searchText or lower(person.address1) like :searchText or lower(person.address2) like :searchText or lower(person.state) like :searchText or lower(person.postalCode) like :searchText)");
			query.append(" or tracking.to in (select person from Person person where lower(person.name) like :searchText or lower(person.country) like :searchText ");
			query.append(" or lower(person.city) like :searchText or lower(person.address1) like :searchText or lower(person.address2) like :searchText or lower(person.state) like :searchText or lower(person.postalCode) like :searchText)))");
		}
		return query.toString();
	}

	@Override
	public List<Tracking> findTrackingsByPerson(final Long idPerson) {
		Query query = getEntityManager().createQuery(bundleQueries.getString("find.trackings.by.person"));
		query.setParameter("idPerson", idPerson);
		return query.getResultList();
	}

	@Override
	public List<Tracking> findByState(final SearchBundle bundle, final State... state) {
		String sqlBundle = buildSearchBundle(bundle);
		Query query = getEntityManager().createQuery(bundleQueries.getString("find.trackings.by.state") + sqlBundle);
		query.setParameter("states", Arrays.asList(state));
		if (!Strings.isEmpty(sqlBundle)) {
			query.setParameter("searchText", "%" + bundle.getSearchText() + "%");
		}
		return query.getResultList();
	}

	@Override
	public List<Tracking> findByNotInState(final SearchBundle bundle, final State... state) {
		String sqlBundle = buildSearchBundle(bundle);
		Query query = getEntityManager().createQuery(
				bundleQueries.getString("find.trackings.by.notinstate") + sqlBundle);
		query.setParameter("states", Arrays.asList(state));
		if (!Strings.isEmpty(sqlBundle)) {
			query.setParameter("searchText", "%" + bundle.getSearchText() + "%");
		}
		return query.getResultList();
	}

	@Override
	public List<Tracking> search(final Tracking tracking) {
		Session session = (Session) getEntityManager().getDelegate();
		Criteria criteria = session.createCriteria(Tracking.class);
		if (tracking.getCode() != null && !"".equals(tracking.getCode())) {
			criteria.add(Restrictions.like("code", "%" + tracking.getCode() + "%").ignoreCase());
		}
		if (tracking.getDescription() != null && !"".equals(tracking.getDescription())) {
			criteria.add(Restrictions.like("description", "%" + tracking.getDescription() + "%").ignoreCase());
		}
		if (tracking.getFrom() != null && tracking.getFrom().getId() != null) {
			criteria.createCriteria("from").add(Restrictions.eq("id", tracking.getFrom().getId()));
		}
		if (tracking.getTo() != null && tracking.getTo().getId() != null) {
			criteria.createCriteria("to").add(Restrictions.eq("id", tracking.getTo().getId()));
		}
		if (tracking.getService() != null && tracking.getService().getId() != null) {
			criteria.createCriteria("service").add(Restrictions.eq("id", tracking.getService().getId()));
		} else {
			if (tracking.getService() != null && tracking.getService().getPostalService() != null
					&& tracking.getService().getPostalService().getId() != null) {
				criteria.createCriteria("service.postalService").add(
						Restrictions.eq("id", tracking.getService().getPostalService().getId()));
			}
		}
		if (tracking.getState() != null) {
			criteria.add(Restrictions.eq("state", tracking.getState()));
		}
		if (tracking.getSent() != null) {
			criteria.add(Restrictions.eq("sent", tracking.getSent()));
		}
		if (tracking.isArchived()) {
			criteria.add(Restrictions.eq("archived", tracking.isArchived()));
		}
		if (tracking.isTaxed()) {
			criteria.add(Restrictions.eq("taxed", tracking.isTaxed()));
		}
		if (tracking.isImportant()) {
			criteria.add(Restrictions.eq("important", tracking.isImportant()));
		}
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	public Long countDeliveredTrackings() {
		Session session = (Session) getEntityManager().getDelegate();
		Criteria criteria = session.createCriteria(Tracking.class);
		criteria.setProjection(Projections.rowCount());
		criteria.add(Restrictions.eq("state", State.Delivered));
		return (Long) criteria.uniqueResult();
	}

	public Long countInTransitTrackings() {
		Session session = (Session) getEntityManager().getDelegate();
		Criteria criteria = session.createCriteria(Tracking.class);
		criteria.setProjection(Projections.rowCount());
		criteria.add(Restrictions.ne("state", State.Unregistered));
		criteria.add(Restrictions.ne("state", State.Delivered));
		criteria.add(Restrictions.ne("state", State.Returned));
		criteria.add(Restrictions.ne("state", State.InternationalReturn));
		return (Long) criteria.uniqueResult();
	}

	public Long countUnregisteredTrackings() {
		Session session = (Session) getEntityManager().getDelegate();
		Criteria criteria = session.createCriteria(Tracking.class);
		criteria.setProjection(Projections.rowCount());
		criteria.add(Restrictions.eq("state", State.Unregistered));
		return (Long) criteria.uniqueResult();
	}

	public Long countNotArchivedTrackings() {
		Session session = (Session) getEntityManager().getDelegate();
		Criteria criteria = session.createCriteria(Tracking.class);
		criteria.setProjection(Projections.rowCount());

		criteria.add(Restrictions.eq("archived", false));
		return (Long) criteria.uniqueResult();
	}

	@Override
	public int countTrackingsByPerson(final Long id) {
		Query query = getEntityManager().createQuery(bundleQueries.getString("count.trackings.by.person"));
		query.setParameter("idPerson", id);
		return ((Long) query.getSingleResult()).intValue();
	}

}
