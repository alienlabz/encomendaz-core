package com.alienlabz.encomendaz.exception;

/**
 * Exceção que informa um erro no processamento do backup do sistema.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class BackupException extends RuntimeException {

	public BackupException() {
		super();
	}

	public BackupException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public BackupException(String arg0) {
		super(arg0);
	}

	public BackupException(Throwable arg0) {
		super(arg0);
	}

}
