package com.alienlabz.encomendaz.exception;

/**
 * Exceção lançada sempre que há uma quantidade pequena de parâmetros para o método
 * realizar suas operações.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class InsufficientParameters extends RuntimeException {

}
