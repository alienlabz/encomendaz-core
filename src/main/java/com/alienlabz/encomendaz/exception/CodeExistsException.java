package com.alienlabz.encomendaz.exception;

/**
 * Exceção lançada quando há a tentativa de inserir um código de rastreamento que já se encontra cadastrado.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class CodeExistsException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public CodeExistsException() {
		super();
	}

	public CodeExistsException(String message, Throwable cause) {
		super(message, cause);
	}

	public CodeExistsException(String message) {
		super(message);
	}

	public CodeExistsException(Throwable cause) {
		super(cause);
	}

}
