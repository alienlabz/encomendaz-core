package com.alienlabz.encomendaz.exception;

/**
 * Exceção lançada quando se detecta a tentativa de remover uma pessoa que já está
 * associada a uma encomenda.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class PersonAssociatedException extends RuntimeException {
	private static final long serialVersionUID = 1L;

}
