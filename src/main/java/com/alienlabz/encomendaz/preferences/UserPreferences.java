package com.alienlabz.encomendaz.preferences;

import com.alienlabz.encomendaz.domain.EmailServerSecurity;

/**
 * Comportamento esperado para objetos que tratam de preferências do sistema.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public interface UserPreferences {

	/**
	 * Obter o som que será tocado para notificar o usuário sobre algo.
	 * 
	 * @return Caminho para o arquivo de áudio.
	 */
	String getNotificationSound();

	/**
	 * Definir o som que será tocado para notificar o usuário sobre algo.
	 * 
	 * @param sound Caminho para o arquivo de áudio.
	 */
	void setNotificationSound(String sound);

	/**
	 * Obter a taxa de atualização das encomendas.
	 * 
	 * @return Taxa de atualização.
	 */
	int getRefreshRate();

	/**
	 * Definir a taxa de atualização das encomendas.
	 * 
	 * @param refreshRate Taxa de atualização.
	 */
	void setRefreshRate(int refreshRate);

	/**
	 * Obter o servidor de e-mail que será usado para enviar e-mails de notificação.
	 * 
	 * @return Servidor de e-mail.
	 */
	String getEmailServer();

	/**
	 * Definir o servidor de e-mail que será usado para enviar e-mails de notificação.
	 * 
	 * @param emailServer Servidor de e-mail.
	 */
	void setEmailServer(String emailServer);

	/**
	 * Obter a porta do servidor de e-mail que será usado para enviar e-mails de notificação.
	 * 
	 * @return Porta do servidor de e-mail.
	 */
	int getEmailServerPort();

	/**
	 * Definir a porta do servidor de e-mail que será usado para enviar e-mails de notificação.
	 * 
	 * @param emailServerPort Porta do servidor de e-mail.
	 */
	void setEmailServerPort(int emailServerPort);

	/**
	 * Obter o nome do usuário no servidor de e-mail.
	 * 
	 * @return Nome do usuário.
	 */
	String getEmailUsername();

	/**
	 * Definir o nome do usuário no servidor de e-mail.
	 * 
	 * @param username Nome do usuário.
	 */
	void setEmailUsername(String username);

	/**
	 * Obter a senha do usuário no servidor de e-mail.
	 * 
	 * @return Senha do usuário.
	 */
	String getEmailPassword();

	/**
	 * Definir a senha do usuário no servidor de e-mail.
	 * 
	 * @param password Senha do usuário.
	 */
	void setEmailPassword(String password);

	/**
	 * Obter o tipo de segurança que é usada no servidor de e-mail.
	 * 
	 * @return Tipo de segurança.
	 */
	EmailServerSecurity getEmailSecurity();

	/**
	 * Definir o tipo de segurança que é usada no servidor de e-mail.
	 * 
	 * @param security Tipo de segurança.
	 */
	void setEmailSecurity(EmailServerSecurity security);

	/**
	 * Obter o caminho para o arquivo de template de e-mail.
	 * 
	 * @return Caminho para o arquivo de template.
	 */
	String getEmailTemplate();

	/**
	 * Definir o caminho para o arquivo de template de e-mail
	 * . 
	 * @param template Caminho para o arquivo de template.
	 */
	void setEmailTemplate(String template);

	/**
	 * Obter o usuário do servidor de proxy.
	 * 
	 * @return Usuário do servidor de proxy.
	 */
	String getProxyUsername();

	/**
	 * Definir o usuário do servidor de proxy.
	 * 
	 * @param username Usuário do servidor de proxy.
	 */
	void setProxyUsername(String username);

	/**
	 * O e-mail que será usado no campo FROM do e-mail.
	 * 
	 * @param sender E-mail.
	 */
	public void setEmailSender(String sender);

	/**
	 * Retornar o e-mail que será usado no campo FROM do e-mail.
	 * 
	 * @return E-mail.
	 */
	public String getEmailSender();

	/**
	 * O assunto do e-mail.
	 * 
	 * @return Assunto.
	 */
	public String getEmailSubject();

	/**
	 * Obter o assunto do e-mail.
	 * 
	 * @param subject Assunto.
	 */
	public void setEmailSubject(String subject);

	/**
	 * Obter a skin padrão da aplicação.
	 * 
	 * @return Skin padrão.
	 */
	public String getDefaultSkin();

	/**
	 * Definir qual a skin padrão da aplicação.
	 * 
	 * @param skin Skin.
	 */
	public void setDefaultSkin(String skin);

	/**
	 * Verificar se é para usar proxy.
	 * 
	 * @return Verdadeiro, caso sim.
	 */
	public boolean isUseProxy();

	/**
	 * Definir se é para usar proxy.
	 * 
	 * @param use Verdadeiro, caso seja para usar proxy.
	 */
	public void setUseProxy(boolean use);

	/**
	 * Obter a senha de acesso ao proxy.
	 * 
	 * @return Senha.
	 */
	public String getProxyPassword();

	/**
	 * Definir a senha de acesso ao proxy.
	 * 
	 * @param pas Senha.
	 */
	public void setProxyPassword(String pas);

	public boolean isSendAutomaticEmails();
	
	public Long getDefaultFrom();

	public void setDefaultFrom(Long id);

	public Long getDefaultTo();

	public void setDefaultTo(Long id);

	public void setSendAutomaticEmails(final boolean send);

	void store();
}
