package com.alienlabz.encomendaz.util;

/**
 * Classe utilitária para Strings.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
final public class StringUtil {

	private StringUtil() {
	}

	/**
	 * Verificar se uma string é vazia ou nula.
	 * 
	 * @param s String a ser verificada
	 * @return Verdadeiro caso seja nula ou vazia.
	 */
	public static boolean isNullOrEmpty(final String s) {
		boolean result = false;
		if (s == null || "".equals(s)) {
			result = true;
		}
		return result;
	}

}
