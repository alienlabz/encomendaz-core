package com.alienlabz.encomendaz.util;

import java.io.StringReader;
import java.net.URLEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.alfredlibrary.utilitarios.net.WorldWideWeb;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

/**
 * Utilitário para obter informações sobre geolocalização.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class GeoLocationUtil {

	/**
	 * Obter a Geo localização a partir do nome de um local usando a API do Yahoo.
	 * 
	 * @param location Localização.
	 * @return GeoLocalização no formato de um array de String. Primeira posição é a latitude. A segunda, a longitude.
	 */
	@SuppressWarnings("deprecation")
	public static String[] getGeoLocationFromYahoo(String location) {
		String[] result = new String[2];

		//FIXME  URLEncoder.encode(location) DEPRECIADO
		String url = "http://where.yahooapis.com/geocode?q=" + URLEncoder.encode(location) + "&appid=Yday566q";
		String conteudo = WorldWideWeb.obterConteudoSite(url, "UTF-8");
		DocumentBuilder builder;
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(conteudo)));
			Node nodeError = doc.getElementsByTagName("Error").item(0);
			if (nodeError != null && "0".equals(nodeError.getTextContent())) {
				result[0] = doc.getElementsByTagName("latitude").item(0).getTextContent();
				result[1] = doc.getElementsByTagName("longitude").item(0).getTextContent();
			}
		} catch (Throwable e) {
		}
		return result;
	}

}
