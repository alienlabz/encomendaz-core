package com.alienlabz.encomendaz.util;

import java.io.StringWriter;
import java.util.Properties;

import javax.inject.Inject;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.slf4j.Logger;

import br.gov.frameworkdemoiselle.annotation.Name;
import br.gov.frameworkdemoiselle.util.ResourceBundle;

import com.alienlabz.encomendaz.domain.EmailServerSecurity;
import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.exception.EmailNotSentException;
import com.alienlabz.encomendaz.preferences.UserPreferences;

/**
 * Utilitário para o envio de e-mails.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
final public class MailSender {

	@Inject
	private UserPreferences prefs;

	@Inject
	private Logger logger;

	@Inject
	@Name("messages")
	private ResourceBundle bundleMessages;

	public void send(String server, int port, String username, String password, EmailServerSecurity security,
			String fromto) throws EmailException {
		HtmlEmail email = new HtmlEmail();
		email.setHostName(server);
		email.setSmtpPort(port);
		email.setAuthenticator(new DefaultAuthenticator(username, password));

		if (security == EmailServerSecurity.SSL_TLS) {
			email.setTLS(true);
			email.setSSL(true);
		} else if (security == EmailServerSecurity.STARTTLS) {
			email.setTLS(true);
		}

		try {
			email.setFrom(fromto);
			email.setSubject(bundleMessages.getString("test.email.subject"));
			email.setHtmlMsg(bundleMessages.getString("test.email.body"));
			email.addTo(fromto);
			email.send();
		} catch (EmailException e) {
			logger.error("problemas ao enviar o e-mail", e);
			throw e;
		}
	}

	/**
	 * Notificar os interessados sobre a mudança de situação da encomenda.
	 * 
	 * @param tracking Encomenda.
	 * @param to Pessoa para quem será enviado o e-mail.
	 */
	public void notify(Tracking tracking, Person to) {
		logger.debug("enviar notificações de e-mail para " + to.getName());

		if (!ObjectUtil.isNullOrEmpty(to, prefs.getEmailServer(), prefs.getEmailSender(), prefs.getEmailSubject(),
				to.getEmail())) {

			logger.debug("enviando e-mail usando o servidor " + prefs.getEmailServer() + " com a porta "
					+ prefs.getEmailServerPort());

			HtmlEmail email = new HtmlEmail();
			email.setHostName(prefs.getEmailServer());
			email.setSmtpPort(prefs.getEmailServerPort());
			email.setAuthenticator(new DefaultAuthenticator(prefs.getEmailUsername(), prefs.getEmailPassword()));

			if (prefs.getEmailSecurity() == EmailServerSecurity.SSL_TLS) {
				email.setTLS(true);
				email.setSSL(true);
			} else if (prefs.getEmailSecurity() == EmailServerSecurity.STARTTLS) {
				email.setTLS(true);
			}

			try {
				email.setFrom(prefs.getEmailSender());
				email.setSubject(prefs.getEmailSubject());
				email.setHtmlMsg(getTemplate(tracking, to));
				email.addTo(to.getEmail());
				email.send();
			} catch (EmailException e) {
				logger.error("problemas ao enviar o e-mail", e);
			}
		}
	}

	/**
	 * Obter o arquivo de template de e-mail e fazer as substituições das chaves.
	 * 
	 * @param tracking Encomenda.
	 * @param person Pessoa para quem o e-mail será enviado.
	 * @return Texto da mensagem já com as devidas substituições.
	 */
	private String getTemplate(final Tracking tracking, final Person person) {
		String template = prefs.getEmailTemplate();
		VelocityContext context = new VelocityContext();
		context.put("encomenda", tracking);
		StringWriter w = new StringWriter();

		final Properties properties = new Properties();
		if (template == null || "".equals(template)) {
			properties.setProperty(VelocityEngine.RESOURCE_LOADER, "classpath");
			properties.put("classpath." + Velocity.RESOURCE_LOADER + ".class", ClasspathResourceLoader.class.getName());
			template = "email.vm";
		} else {
			properties.put("file.resource.loader.path", getPath(template));
			properties.put("resource.loader", "file");
			properties.put("file.resource.loader.class",
					"org.apache.velocity.runtime.resource.loader.FileResourceLoader");
			template = template.substring(template.lastIndexOf('/') + 1);
		}
		VelocityEngine ve = new VelocityEngine();
		ve.init(properties);

		try {
			ve.mergeTemplate(template, "UTF-8", context, w);
		} catch (Throwable t) {
			throw new EmailNotSentException();
		}
		return w.toString();
	}

	private Object getPath(final String template) {
		return template.substring(0, template.lastIndexOf('/'));
	}

}
