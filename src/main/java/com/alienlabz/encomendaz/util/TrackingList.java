package com.alienlabz.encomendaz.util;

import java.util.List;


import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import com.alienlabz.encomendaz.domain.Tracking;

@Root
@Default(value = DefaultType.FIELD)
public class TrackingList {

	@ElementList
	private List<Tracking> trackings;

	public List<Tracking> getTrackings() {
		return trackings;
	}

	public void setTrackings(List<Tracking> trackings) {
		this.trackings = trackings;
	}

}
