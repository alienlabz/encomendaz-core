package com.alienlabz.encomendaz.business;

import javax.inject.Inject;

import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import br.gov.frameworkdemoiselle.template.DelegateCrud;

import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.exception.PersonAssociatedException;
import com.alienlabz.encomendaz.persistence.PersonPersistence;
import com.alienlabz.encomendaz.preferences.UserPreferences;

/**
 * Trata as regras de negócio relacionadas à entidade de Pessoa.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@BusinessController
public class PersonBC extends DelegateCrud<Person, Long, PersonPersistence> {
	private static final long serialVersionUID = 1L;

	@Inject
	private TrackingBC trackingBC;

	@Inject
	private UserPreferences userPrefs;

	/**
	 * Inserir uma pessoa. Caso ela já exista, a inserção não ocorre e não será levantada nenhuma exceção.
	 * 
	 * @param person Pessoa a ser inserida.
	 */
	public void insertIfNotExists(Person person) {
		getDelegate().insertIfNotExists(person);
	}

	/**
	 * Verificar se uma determinada pessoa possui rastreamentos associados a ela.
	 * 
	 * @param person Pessoa a ser verificada.
	 * @return Se existe rastreamento associado a esta pessoa.
	 */
	public boolean existsPersonByTracking(Person person) {
		return trackingBC.existsTrackingsByPerson(person.getId());
	}

	/**
	 * Forçar a remoção de uma pessoa, mesmo que haja violações de relacionamento.
	 * 
	 * @param id Identificador.
	 */
	public void forceDelete(Long id) {
		trackingBC.updateTrackingsPeopleToNull(id);
		delete(id);
	}

	/**
	 * Obter quem é a pessoa que será tratada como remetente padrão.
	 * 
	 * @return Remetente padrão.
	 */
	public Person getDefaultFrom() {
		Person result = null;
		Long id = userPrefs.getDefaultFrom();
		if (id != 0) {
			result = load(id);
		}
		return result;
	}

	/**
	 * Obter quem é a pessoa que será tratada como destinatário padrão.
	 * 
	 * @return Destinatário padrão.
	 */
	public Person getDefaultTo() {
		Person result = null;
		Long id = userPrefs.getDefaultTo();
		if (id != 0) {
			result = load(id);
		}
		return result;
	}

	/**
	 * Definir a pessoa que será tratada como remetente padrão.
	 * 
	 * @param person Remetente padrão.
	 */
	public void setDefaultFrom(Person person) {
		if (person != null && person.getId() != null) {
			userPrefs.setDefaultFrom(person.getId());
		}
	}

	/**
	 * Definir quem é a pessoa que será tratada como destinatário padrão.
	 * 
	 * @param person Destinatário padrão.
	 */
	public void setDefaultTo(Person person) {
		if (person != null && person.getId() != null) {
			userPrefs.setDefaultTo(person.getId());
		}
	}

	@Override
	public void delete(Long id) {
		try {
			super.delete(id);
		} catch (Exception exception) {
			throw new PersonAssociatedException();
		}
	}

}
